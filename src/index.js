import ForgeUI, { Code, Heading, Fragment, Text, render, useState } from "@forge/ui";
import { webTrigger, storage } from "@forge/api";

// The `listener` function is called when the webtrigger is invoked
export async function listener(req) {
  try {
    const body = JSON.parse(req.body);
    const newmotd = body.motd;
    await storage.set("motd", newmotd);
    return {
      body: "Success: Message updated\n",
      headers: { "Content-Type": ["application/json"] },
      statusCode: 200,
      statusText: "OK",
    };
  } catch (error) {
    return {
      body: error + "\n",
      headers: { "Content-Type": ["application/json"] },
      statusCode: 400,
      statusText: "Bad Request",
    }
  }
}

// useState is needed to wrap functions that return a promise
function AdminPage() {
  const [motd] = useState(storage.get("motd"));
  const [trigger] = useState(webTrigger.getUrl("motd-listener"));
  const data = JSON.stringify({ motd: "Hello, world!" });
  const sampleCurl = `curl -X POST ${trigger} --data '${data}'`;

  return (
    <Fragment>
      <Heading>Set today's message</Heading>
      <Text>To set the message, run the following curl command, then refresh this page.</Text>
      <Code text={sampleCurl} language="shell" />
      <Heading>View today's message</Heading>
      <Text>{motd ? motd : "Message not set" }</Text>
    </Fragment>
  );
}

// The `display` function is called when the admin page is loaded
export const display = render(
  <AdminPage />
);